from django.contrib import admin

# Register your models here.
from .models import Admin, Support, Developer, Category, Application, User, Icon, Media, Keyword, Review, UserPayment, DeveloperInPayment, DeveloperOutPayment 

admin.site.register(Admin)
admin.site.register(Support)
admin.site.register(Developer)
admin.site.register(Category)
admin.site.register(Application)
admin.site.register(User)
admin.site.register(Icon)
admin.site.register(Media)
admin.site.register(Keyword)
admin.site.register(Review)
admin.site.register(UserPayment)
admin.site.register(DeveloperInPayment)
admin.site.register(DeveloperOutPayment)